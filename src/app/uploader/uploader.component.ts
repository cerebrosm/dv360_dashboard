import { Component, OnInit, ViewChild  } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent implements OnInit {

  constructor(private storageService: StorageService, private ngxCsvParser: NgxCsvParser, private calendar: NgbCalendar) { }
  ngOnInit(): void {
  }

  //********************* Datepicker proccess *********************
  model!: NgbDateStruct;
  date!: { year: number; month: number; };

  selectToday() {
    this.model = this.calendar.getToday();
  }

  
  //********************* NgxCsvParser proccess *********************
  header: boolean = true;

  uploadFiles(event: any){

    //GET FILES FROM INPUT
    let files = event.target.files;
 
    this.ngxCsvParser.parse(files[0], { header: this.header, delimiter: ',' })
      .pipe().subscribe({
        next: (result): void => {

          const blob = new Blob([JSON.stringify(result)], {type : 'application/json'});

          let reader = new FileReader();
          reader.readAsDataURL(blob);
          
          reader.onloadend = () => {
            let name = files[0].name.split('.')[0];
            
            /*MILISEGUNDO ACTUAL
            this.storageService.uploadFile(name + "_"+ Date.now(), reader.result).then(response =>{
              console.log(response);
            });*/

            this.storageService.uploadFile(name, reader.result).then(response =>{
              console.log(response);
            });
            
          }

        },
        error: (error: NgxCSVParserError): void => {
          console.log('Error', error);
        }
      });

    /*
    let reader = new FileReader();

    reader.readAsDataURL(files[0]);
    reader.onloadend = () => {
      let name = files[0].name.split('.')[0];
      console.log(reader.result);
      //this.csv.push(reader.result);
      this.storageService.uploadFile(name + "_"+ Date.now(), reader.result).then(response =>{
        console.log(response);
      });
      
    }*/


  }

}
