import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app';
import 'firebase/compat/storage';
import { environment } from 'src/environments/environment';

firebase.initializeApp(environment.firebaseConfig);

@Injectable({
  providedIn: 'root'
})

export class StorageService {
  storageRef = firebase.app().storage().ref();
  constructor() { }

  async uploadFile(name: string, fileB64: any){
    try{
      let response = this.storageRef.child("DV360_CEDR/original_files/" + name).putString(fileB64, "data_url");
      return (await response).ref.getDownloadURL();
    } catch(err){
      console.log(err);
      return null;
    }
  }
}
