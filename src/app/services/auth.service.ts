import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { signInWithEmailAndPassword } from '@firebase/auth';
import firebase from 'firebase/compat/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afAuth: AngularFireAuth) { }

 async login(email:string, password:string){
   
    try {
      return await this.afAuth.signInWithEmailAndPassword(email, password);
    } catch (error) {
      console.log("Error al logearse");
      return error;
    }
    /*
    await this.afAuth.signInWithEmailAndPassword(email, password).then(
      response => {
        return {status:true, response};
      }
    ).catch(
      error => {
        return {status:false, error};
      }
    );*/
  }

  async register(email:string, password:string){
    try {
      return await this.afAuth.createUserWithEmailAndPassword(email, password);
    } catch (error) {
      console.log("Error al crear usuario: ", error);
      return null;
    }
  }

  async loginWithGoogle(email:string, password:string){
    try {
      return await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    } catch (error) {
      console.log("Error login with Google: ", error);
      return null;
    }
  }

  getLogedUser(){
   return this.afAuth.authState; 
  }

  logout(){
    this.afAuth.signOut();
  }
}
