import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private authService:AuthService, private router: Router) { }

  userLoged: any;

  ngOnInit(): void {
    this.authService.getLogedUser().subscribe( user => {
      this.userLoged = user;
      if(this.userLoged == null){
        this.router.navigate(['/login']);
      }
    })
  }

}
