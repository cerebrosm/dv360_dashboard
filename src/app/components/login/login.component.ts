import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title = 'DV360';
  //faCoffee = faCoffee;
  usuario = {
    email: '',
    password:''
  };

  constructor(private authService: AuthService, private router: Router){

  }

  Ingresar() {
    console.log(this.usuario);
    const {email, password} = this.usuario;

    /*
    this.authService.register(email, password).then( res => {
      console.log("Se registro: ", res);
    });
    */

    this.authService.login(email, password).then( res => {
      console.log("Se logeo: ", res);
    });
    
  }

  IngresarConGoogle() {
    const {email, password} = this.usuario;

    this.authService.loginWithGoogle(email, password).then( res => {

      if(res?.user && res.user.uid != null){
        //console.log("Se logeo: ", res);
        this.router.navigate(['/home']);
      }
    });
    
  }

  /*
  getLogedUser(){
    this.authService.getLogedUser().subscribe( res => {
      console.log(res?.email);
    })
  }
 */
 
  logout(){
    this.authService.logout();
  }


  ngOnInit(): void {
  }

}
