import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";



const routes: Routes = [
    {
        path: "login",
        component: LoginComponent
    },
    {
        pathMatch: 'full',
        path: "",
        redirectTo: "home"
    },
    {
        path: "home",
        component: HomeComponent
    },
    {
        path: "**",
        component: PageNotFoundComponent
    }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


/*
@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
*/
